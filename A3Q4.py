# Name: Assignment 3 Question 4
# Time: 13:07 11.6.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to find the size of number by recursion
# Created by D.python

def func(x):                                 # RECURSIVE FUNCTION
    """""
    The purpose of this function is to
    find out how many digits is used 
    in the input, using recursion
    """""
    if x < 10:                               # IF SMALLER THAN 10
        return 1                             # IF SO, THEN RUN THIS
    else:
        return 1 + func(x / 10)              # OTHERWISE RUN THIS

num = int(input("Enter you input please: ")) # TO ENTER AN INPUT AS AN INT HERE
print(func(num))                             # TO CALL UNC AND GIVE THE OUTPUT