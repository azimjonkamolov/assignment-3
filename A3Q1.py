# Name: Assignment 3 Question 1
# Time: 12:26 11.6.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to get strings but not commas
# Created by D.python

raq = input()               # TO GET THE INPUT AS A STRING
aa=','                      # TO GET A COMMA INT A VARABLE USED DOWN THERE
l=[]                        # TO CREATE A LIST AS TUPLE CANNOT BE APPENDED
lol = ""                    # TO SEPARATE NUMS WITH COMMAS
for i in raq:               # TO GET VALUES IN RAQ
    if i != aa:             # TO CHECK WHETHER I IS NOT AA OR IT IS (AA IS A COMMA)
        lol=lol+i           # IF NOT, THEN ADD IT TO LOL
    if i==aa:               # TO CHECK WHETHER I IS AA OR NOT
        l.append(lol)       # IF SO LOL IS ADDED INTO THE LIST
        lol=""              # AND LOL IS TURNED INTO AN EMPTY STRING
    if  i==(raq[-1:]):      # TO GET VALUE AFTER THE LAST COMMA
        l.append(lol)       # TO TAKE THE LAST LOL INTO THE LIST
        lol=""              # TO TURN LOL INTO AN EMPTY STRING
tupleit=(tuple(l))          # TO TURN THE LIST INTO A TUPLE
print(tupleit)              # TO GIVE AN OUTPUT
