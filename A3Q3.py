# Name: Assignment 3 Question 3
# Time: 12:49 11.6.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to use frequency and dictionaries
# Created by D.python

def num_of_words(song):                         # TO COUNT THE NUMBER OF WORDS
    """""
    This function's purpose is to 
    count how many times words are repeated
    """""
    print("\t<<<Inside of function N 1>>>")     # TO SHOW WHICH FUNCTION IS THIS IN
    di = dict()                                 # TO CREATE A DICTIONARY WITH DI USING DICT() FUNC
    for w in gangnam_style:                     # TO GET THE WORDS INSIDE GANGNAM_STYLE LIST
        if w in di:                             # TO CHECK WHETHER THE WORD IS IN THE DICTIONAR OR NOT
            di[w] = di[w] + 1                   # IF SO, THEN ADD ONE INTO ITS VALUE
        else:                                   # IF NOT
            di[w] = 1                           # JUST PUT ONE ITS VALUE
        print(w, " >>> ", di[w])                # TO PRINT OUT WORD AND ITS VALUES

def max_and_min(song):                          # TO FIND THE MIN AND MAX WORDS
    """""
    The purpose of this function to 
    find the max used and min used 
    words in the list
    """""
    print("\n\t<<<Inside of function N 2>>>")   # TO SHOW WHICH FUNCTION IS THIS IN
                                                # ABOVE ACTIONS IN THE NUM OF VORS BEING REPEATED
    di = dict()
    for w in gangnam_style:
        if w in di:
            di[w] = di[w] + 1
        else:
            di[w] = 1
    print("The most used word: ",max(zip(di.values(), di.keys())))      # TO FIND THE MAX USED WORD USING FUNCTION MAX
    print("The least used word: ",min(zip(di.values(), di.keys())))     # TO FIND THE LEAST USED WORD USING FUNCTION MIN
                                                                        # THE OUTPUT GIVES THE VALUE FIRS AND THEN THE KEY THOUGH
gangnam_style = ['oppan', 'gangnam-style', 'gangnam-style',
                 'najeneun', 'ttasaroun', 'inkanjeogin', 'yeoja',
                 'keopi', 'hanjanye', 'yeoyureuraneun', 'pumkyeok', 'inneun', 'yeoja',
                 'bami', 'omyeon', 'shimjangi', 'tteugeowojineun', 'yeoja',
                 'keureon', 'banjeon', 'inneun', 'yeoja',

                 'naneun', 'sana-i',
                 'najeneun', 'neomankeum', 'ttasaroun', 'geureon', 'sana-i',
                 'keopi', 'shikgido', 'jeone', 'wonsyas', 'ttaerineun', 'sana-i',
                 'bami', 'omyeon', 'shimjangi', 'teojyeobeorineun', 'sana-i',
                 'keureon', 'sana-i',

                 'areumdawo', 'sarangseureowo',
                 'keurae', 'neo', 'hey', 'keurae', 'baro', 'neo', 'hey',

                 'areumdawo', 'sarangseureowo',
                 'keurae', 'neo', 'hey', 'keurae', 'baro', 'neo', 'hey',

                 'chigeumbuteo', 'kal', 'dekkaji', 'kabol-kka',

                 'oppan', 'gangnam-style', 'gangnam-style',

                 'oppan', 'gangnam-style', 'gangnam-style',

                 'oppan', 'gangnam-style', 'gangnam-style',

                 'hey', 'sexy', 'lady',
                 'oppan', 'gangnam-style', 'gangnam-style',

                 'hey', 'sexy', 'lady']                      # THE LIST

num_of_words(gangnam_style)                                  # TO CALL A FUNCTION
max_and_min(gangnam_style)                                   # TO CALL A FUNCTION
