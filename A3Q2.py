# Name: Assignment 3 Question 2
# Time: 12:35 11.6.2018
# Author: Azimjon Kamolov
# Contact: azimjon.6561@gmail.com
# Purpose: to get unique items from 2 lists with function too
# Created by D.python

def uniqe(l,ll,lll):               # A FUNCTION WITH THREE ARGUMENT VALUES
    """""
    Using a function was a requirement
    so here is the function with 
    three list argumnets in it
    """""
    for i in l:                    # TO MAKE I GET VALUES OF L
        if i not in ll:            # TO CHECK WHETHER I IS NOT IN LL LIST OR IT IS
            lll.append(i)          # IF NOT, THEN I IS ADDED INTO LLL LIST
    for j in ll:                   # TO MAKE J GET VALUES OF LL
        if j not in l:             # TO CHECK WHETHER J IS NOT IN L LIST OF IT IS
            lll.append(j)          # IF NOT, THEN J IS ADDED INTO LLL LIST
    return lll                     # TO RETURN LLL LIST ALONE

a = [12,34,"orange",98,4]          # FIRST LIST
b = ["a",4,27.8,72,"orange"]       # SECOND LIST
c = []                             # AN EMPTY THIRD LIST
print(uniqe(a,b,c))                # TO CALL FINCTION AND PRINT OUT WITH PRINT FUNC



